import { mock } from 'ts-mockito'

/**
 * Mock an object every time a test starts.
 * `setupFn` will be called after creating the mock to setup ts-mockito `when(...).then(...)` handlers.
 */
export function mockBeforeEach<T extends {}> (origin?: { new(...args: any[]): T }, setupFn?: (mock: T) => void): T
export function mockBeforeEach<T extends {}> (origin?: T, setupFn?: (mock: T) => void): T
export function mockBeforeEach<T extends {}> (origin?: any, setupFn?: (mock: T) => void): T
export function mockBeforeEach<T extends {}> (origin?: any, setupFn?: (mock: T) => void): T {
  let value: T = {} as T

  beforeEach(() => {
    value = mock(origin)
    setupFn?.(value)
  })

  return new Proxy(value, {
    get: (_target, key) => {
      if (['Symbol(Symbol.toPrimitive)', 'then', 'catch'].includes(key.toString())) {
        return undefined
      }

      return value[key]
    },
    set: (_target, key, propValue) => {
      if (['Symbol(Symbol.toPrimitive)', 'then', 'catch'].includes(key.toString())) {
        return false
      }

      value[key] = propValue
      return true
    }
  })
}
