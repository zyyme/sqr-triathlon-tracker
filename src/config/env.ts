import * as process from 'process'

export default () => {
  const PORT = process.env.PORT || 4000
  const DB_CREDS = process.env.DB_CREDS || 'mongodb://localhost:27017/triathlon-tracker'
  const JWT_SECRET = process.env.JWT_SECRET || 's00p3r_53cr57'

  return {
    port: PORT,
    jwtSecret: JWT_SECRET,
    db: {
      credentials: DB_CREDS
    }
  }
}
