import { Module } from '@nestjs/common'
import { DbModule } from './db/db.module'
import { ConfigModule } from '@nestjs/config'
import { UsersModule } from './api/users/users.module'
import { UsersController } from './api/users/users.controller'
import { EventsModule } from './api/events/events.module'
import { AuthModule } from './auth/auth.module'
import env from './config/env'

@Module({
  imports: [
    DbModule,
    ConfigModule.forRoot({ isGlobal: true, load: [env] }),
    AuthModule,
    UsersModule,
    EventsModule
  ],
  controllers: [UsersController],
  providers: []
})
export class AppModule {}
