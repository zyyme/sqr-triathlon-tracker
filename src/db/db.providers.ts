import { DB_CONNECTION } from '../utils/injectTokens.utils'
import * as mongoose from 'mongoose'
import { ConfigService } from '@nestjs/config'

export const databaseProviders = [
  {
    provide: DB_CONNECTION,
    useFactory: (configService: ConfigService) =>
      mongoose.connect(configService.get<string>('db.credentials')),
    inject: [ConfigService]
  }
]
