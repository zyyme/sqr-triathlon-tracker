import { DB_CONNECTION, TRAINING_MODEL, USER_MODEL } from '../utils/injectTokens.utils'
import { connectTrainingModel, connectUserModel } from '../schemas'

export function dbAggregation<T = any> (MODEL, factory, databaseConnection) {
  return {
    provide: MODEL,
    useFactory: factory,
    inject: [databaseConnection]
  }
}

export const modelProviders = [
  dbAggregation(USER_MODEL, connectUserModel, DB_CONNECTION),
  dbAggregation(TRAINING_MODEL, connectTrainingModel, DB_CONNECTION)
]
