import { Module } from '@nestjs/common'
import { databaseProviders } from './db.providers'
import { ConfigModule } from '@nestjs/config'
import { modelProviders } from './model.providers'

@Module({
  imports: [ConfigModule],
  providers: [
    ...databaseProviders,
    ...modelProviders
  ],
  exports: [
    ...databaseProviders,
    ...modelProviders
  ]
})
export class DbModule {}
