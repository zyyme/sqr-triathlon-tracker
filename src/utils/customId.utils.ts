import { alphabet } from './consts'
import { customAlphabet } from 'nanoid'

const nanoid = customAlphabet(alphabet, 12)

export const id = {
  type: String,
  maxlength: 36,
  default: () => nanoid()
}
