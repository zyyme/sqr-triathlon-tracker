import { alphabet } from './consts'
import { customAlphabet } from 'nanoid'

const nanoid = customAlphabet('0123456789', 6)

export const friendCode = {
  type: String,
  maxlength: 36,
  default: () => nanoid()
}
