import { EventsController } from './events.controller'
import { EventsService } from './events.service'
import { mockBeforeEach } from '../../../test/test.utils'
import { anything, deepEqual, instance, verify, when } from 'ts-mockito'
import { TrainingClass, TrainingType, TrainingUnits } from '../../schemas'
import { expect } from 'chai'

describe('EventsController', () => {
  const EventServiceMock = mockBeforeEach<EventsService>()

  const trainingEvent: TrainingClass = {
    _id: 'id',
    type: TrainingType.RUNNING,
    distance: 5,
    units: TrainingUnits.METRIC,
    userId: 'userId'
  }

  let controller: EventsController

  beforeEach(() => {
    controller = new EventsController(instance(EventServiceMock))
  })

  describe('#create', () => {
    beforeEach(() => {
      when(EventServiceMock.create(anything(), anything())).thenResolve(trainingEvent)
    })

    it('calls eventsService.create() with correct data', async () => {
      await controller.create({
        type: TrainingType.RUNNING,
        distance: 5,
        units: TrainingUnits.IMPERIAL
      }, {
        user: {
          userId: 'userId',
          username: 'test'
        }
      })

      verify(EventServiceMock.create(
        deepEqual({
          type: TrainingType.RUNNING,
          distance: 5,
          units: TrainingUnits.IMPERIAL
        }),
        deepEqual({
          userId: 'userId',
          username: 'test'
        })
      )).once()
    })

    it('returns correct data', async () => {
      const result = await controller.create({
        type: TrainingType.RUNNING,
        distance: 5,
        units: TrainingUnits.IMPERIAL
      }, {
        user: {
          userId: 'userId',
          username: 'test'
        }
      })

      expect(result).deep.eq(trainingEvent)
    })
  })

  describe('#findAll', () => {
    beforeEach(() => {
      when(EventServiceMock.findAll(anything())).thenResolve([trainingEvent])
    })

    it('calls eventsService.findAll()', async () => {
      await controller.findAll({ user: { userId: 'userId', username: 'test' } })

      verify(EventServiceMock.findAll(deepEqual({ userId: 'userId', username: 'test' }))).once()
    })

    it('returns correct data', async () => {
      const result = await controller.findAll({ user: { userId: 'userId', username: 'test' } })

      expect(result).deep.eq([trainingEvent])
    })
  })

  describe('#findOne', () => {
    beforeEach(() => {
      when(EventServiceMock.findOne(anything(), anything())).thenResolve(trainingEvent)
    })

    it('calls eventsService.findOne()', async () => {
      await controller.findOne('id', { user: { userId: 'userId', username: 'test' } })

      verify(EventServiceMock.findOne('id', deepEqual({ userId: 'userId', username: 'test' }))).once()
    })

    it('returns correct data', async () => {
      const result = await controller.findOne('id', { user: { userId: 'userId', username: 'test' } })

      expect(result).deep.eq(trainingEvent)
    })
  })

  describe('#aggregateEvents', () => {
    beforeEach(() => {
      when(EventServiceMock.aggregateEvents(anything())).thenResolve({
        runningEvents: { total: 0, events: [] },
        swimmingEvents: { total: 0, events: [] },
        cyclingEvents: { total: 0, events: [] }
      })
    })

    it('calls eventsService.aggregateEvents()', async () => {
      await controller.aggregateEvents({ user: { userId: 'userId', username: 'test' } })

      verify(EventServiceMock.aggregateEvents(deepEqual({ userId: 'userId', username: 'test' }))).once()
    })

    it('returns correct data', async () => {
      const result = await controller.aggregateEvents({ user: { userId: 'userId', username: 'test' } })

      expect(result).deep.eq({
        runningEvents: { total: 0, events: [] },
        swimmingEvents: { total: 0, events: [] },
        cyclingEvents: { total: 0, events: [] }
      })
    })
  })
})
