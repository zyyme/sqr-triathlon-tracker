import { EventsService } from './events.service'
import { anything, deepEqual, instance, verify, when } from 'ts-mockito'
import { TrainingClass, TrainingDocument, TrainingModel, TrainingType, TrainingUnits } from '../../schemas'
import { expect } from 'chai'
import { mockBeforeEach } from '../../../test/test.utils'

describe('EventsService', () => {
  const TrainingModelMock = mockBeforeEach<TrainingModel>()

  const trainingEvent: TrainingClass = {
    _id: 'id',
    type: TrainingType.RUNNING,
    distance: 5,
    units: TrainingUnits.METRIC,
    userId: 'userId'
  }

  let service: EventsService

  beforeEach(() => {
    service = new EventsService(instance(TrainingModelMock))
  })

  describe('#create', () => {
    beforeEach(() => {
      when(TrainingModelMock.create(anything())).thenResolve(trainingEvent as TrainingDocument)
    })

    it('calls trainingModel.create', async () => {
      await service.create(
        { type: TrainingType.RUNNING, distance: 5, units: TrainingUnits.METRIC },
        { userId: 'userId', username: 'test' }
      )

      verify(TrainingModelMock.create(deepEqual({
        userId: 'userId',
        type: TrainingType.RUNNING,
        distance: 5,
        units: TrainingUnits.METRIC
      }))).once()
    })

    it('returns created object properly', async () => {
      const result = await service.create(
        { type: TrainingType.RUNNING, distance: 5, units: TrainingUnits.METRIC },
        { userId: 'userId', username: 'test' }
      )

      expect(result).deep.eq(trainingEvent)
    })
  })

  describe('#findAll', () => {
    beforeEach(() => {
      when(TrainingModelMock.find(anything())).thenResolve([trainingEvent] as any)
    })

    it('calls trainingModel.find()', async () => {
      await service.findAll({ userId: 'userId', username: 'test' })

      verify(TrainingModelMock.find(deepEqual({ userId: 'userId' }))).once()
    })

    it('returns correct results', async () => {
      const result = await service.findAll({ userId: 'userId', username: 'test' })
      expect(result).deep.eq([trainingEvent])
    })
  })

  describe('#findOne', () => {
    beforeEach(() => {
      when(TrainingModelMock.findById(anything())).thenResolve(trainingEvent as any)
    })

    it('calls trainingModel.findById', async () => {
      await service.findOne('id', { userId: 'userId', username: 'username' })

      verify(TrainingModelMock.findById('id')).once()
    })

    it('returns correct data if userId is correct', async () => {
      const result = await service.findOne('id', { userId: 'userId', username: 'username' })

      expect(result).deep.eq(trainingEvent)
    })

    it('throws if userId from request does not match with userId from document', async () => {
      try {
        await service.findOne('id', { userId: 'imposter', username: 'imposter' })
      } catch (e) {
        expect(e.response.statusCode).eq(401)
        expect(e.response.message).eq('Unauthorized')
      }
    })

    it('throws if document not found', async () => {
      when(TrainingModelMock.findById('fakeId')).thenResolve(null)

      try {
        await service.findOne('fakeId', { userId: 'userId', username: 'username' })
      } catch (e) {
        expect(e.response.statusCode).eq(404)
        expect(e.response.message).eq('Not Found')
      }
    })
  })

  describe('#aggregateEvents', () => {
    beforeEach(() => {
      when(TrainingModelMock.find(anything())).thenResolve([
        trainingEvent,
        { ...trainingEvent, type: TrainingType.SWIMMING },
        { ...trainingEvent, type: TrainingType.CYCLING }
      ] as any)
    })

    it('calls service.findAll()', async () => {
      await service.aggregateEvents({ userId: 'userId', username: 'username' })

      verify(TrainingModelMock.find(deepEqual({ userId: 'userId' }))).once()
    })

    it('returns correct data', async () => {
      const result = await service.aggregateEvents({ userId: 'userId', username: 'username' })

      expect(result).deep.eq({
        runningEvents: { total: 5, events: [trainingEvent] },
        cyclingEvents: { total: 5, events: [{ ...trainingEvent, type: TrainingType.CYCLING }] },
        swimmingEvents: { total: 5, events: [{ ...trainingEvent, type: TrainingType.SWIMMING }] }
      })
    })
  })
})
