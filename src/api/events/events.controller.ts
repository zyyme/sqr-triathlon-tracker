import { Controller, Get, Post, Body, Param, Delete, UseGuards, Req } from '@nestjs/common'
import { EventsService } from './events.service'
import { CreateEventDto } from './dto/create-event.dto'
import { ApiBearerAuth } from '@nestjs/swagger'
import { JwtAuthGuard } from '../../auth/jwt-auth.guard'
import { AggregatedEventsDto } from './dto/aggregated-events.dto'
import { TrainingClass } from '../../schemas'

@Controller('events')
export class EventsController {
  constructor (private readonly eventsService: EventsService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post()
  create (@Body() createEventDto: CreateEventDto, @Req() req) {
    return this.eventsService.create(createEventDto, req.user)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get()
  findAll (@Req() req): Promise<TrainingClass[]> {
    return this.eventsService.findAll(req.user)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('stats')
  aggregateEvents (@Req() req): Promise<AggregatedEventsDto> {
    return this.eventsService.aggregateEvents(req.user)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne (@Param('id') id: string, @Req() req): Promise<TrainingClass> {
    return this.eventsService.findOne(id, req.user)
  }
}
