import { Module } from '@nestjs/common'
import { EventsService } from './events.service'
import { EventsController } from './events.controller'
import { DbModule } from '../../db/db.module'
import { ConfigModule } from '@nestjs/config'
import { AuthModule } from '../../auth/auth.module'

@Module({
  imports: [
    ConfigModule,
    DbModule,
    AuthModule
  ],
  controllers: [EventsController],
  providers: [EventsService]
})
export class EventsModule {}
