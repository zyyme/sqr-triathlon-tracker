import { ApiProperty } from '@nestjs/swagger'
import { TrainingType, TrainingUnits } from '../../../schemas'
import { IsNumber, IsPositive, IsString } from 'class-validator'

export class CreateEventDto {
  @ApiProperty({ enum: TrainingType })
  type: TrainingType

  @ApiProperty()
  @IsNumber()
  @IsPositive()
  distance: number

  @ApiProperty({ enum: TrainingUnits })
  units: TrainingUnits
}
