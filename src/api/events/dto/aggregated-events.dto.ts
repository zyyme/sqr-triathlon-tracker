import { ApiProperty } from '@nestjs/swagger'
import { TrainingClass, TrainingType } from '../../../schemas'

export class AggregatedEvent {
  @ApiProperty()
  total: number

  @ApiProperty({ type: TrainingClass })
  events: TrainingClass[]
}

export class AggregatedEventsDto {
  @ApiProperty({ type: AggregatedEvent })
  runningEvents: AggregatedEvent

  @ApiProperty({ type: AggregatedEvent })
  cyclingEvents: AggregatedEvent

  @ApiProperty({ type: AggregatedEvent })
  swimmingEvents: AggregatedEvent
}
