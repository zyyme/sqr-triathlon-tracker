import { Inject, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common'
import { CreateEventDto } from './dto/create-event.dto'
import { TrainingClass, TrainingModel, TrainingType } from '../../schemas'
import { TRAINING_MODEL } from '../../utils/injectTokens.utils'
import { AggregatedEventsDto } from './dto/aggregated-events.dto'

@Injectable()
export class EventsService {
  constructor (
    @Inject(TRAINING_MODEL) private readonly trainingModel: TrainingModel
  ) {}

  async create (createEventDto: CreateEventDto, user: { username: string, userId: string }): Promise<TrainingClass> {
    console.log({ ...createEventDto, userId: user.userId })
    return this.trainingModel.create({ ...createEventDto, userId: user.userId })
  }

  async findAll (user: { username: string, userId: string }): Promise<TrainingClass[]> {
    return this.trainingModel.find({ userId: user.userId })
  }

  async findOne (id: string, user: { username: string, userId: string }): Promise<TrainingClass> {
    const event = await this.trainingModel.findById(id)

    if (event) {
      if (event.userId === user.userId) {
        return event
      }
      throw new UnauthorizedException()
    }

    throw new NotFoundException()
  }

  async aggregateEvents (user: { username: string, userId: string }): Promise<AggregatedEventsDto> {
    const userEvents = await this.findAll(user)

    const aggregatedEvents: AggregatedEventsDto = {
      runningEvents: { total: 0, events: [] },
      cyclingEvents: { total: 0, events: [] },
      swimmingEvents: { total: 0, events: [] }
    }

    for (const event of userEvents) {
      if (event.type === TrainingType.RUNNING) {
        aggregatedEvents.runningEvents.total += event.distance
        aggregatedEvents.runningEvents.events.push(event)
      } else if (event.type === TrainingType.CYCLING) {
        aggregatedEvents.cyclingEvents.total += event.distance
        aggregatedEvents.cyclingEvents.events.push(event)
      } else {
        aggregatedEvents.swimmingEvents.total += event.distance
        aggregatedEvents.swimmingEvents.events.push(event)
      }
    }

    return aggregatedEvents
  }
}
