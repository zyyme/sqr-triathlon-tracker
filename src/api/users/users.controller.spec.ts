import { UsersController } from './users.controller'
import { mockBeforeEach } from '../../../test/test.utils'
import { UsersService } from './users.service'
import { anything, deepEqual, instance, verify, when } from 'ts-mockito'
import { expect } from 'chai'

describe('AuthController', () => {
  const UsersServiceMock = mockBeforeEach<UsersService>()

  let controller: UsersController

  const userObject = {
    _id: 'id',
    email: 'email',
    name: 'name',
    runningGoal: 0,
    cyclingGoal: 0,
    swimmingGoal: 0,
    friendCode: '123123',
    friends: []
  }

  beforeEach(() => {
    controller = new UsersController(instance(UsersServiceMock))
  })

  describe('#getUser', () => {
    beforeEach(() => {
      when(UsersServiceMock.getUser(anything())).thenResolve(userObject)
    })

    it('calls userService.getUser()', async () => {
      await controller.getUser({ user: { userId: 'id', username: 'test' } })

      verify(UsersServiceMock.getUser('id')).once()
    })

    it('returns correct data', async () => {
      const result = await controller.getUser({ user: { userId: 'id', username: 'test' } })

      expect(result).deep.eq(userObject)
    })
  })

  describe('#login', () => {
    beforeEach(() => {
      when(UsersServiceMock.login(anything(), anything())).thenResolve({ access_token: 'access' })
    })

    it('calls userService.login()', async () => {
      await controller.login({ email: 'email', password: 'password' })

      verify(UsersServiceMock.login('email', 'password')).once()
    })

    it('returns correct data', async () => {
      const result = await controller.login({ email: 'email', password: 'password' })

      expect(result).deep.eq({ access_token: 'access' })
    })
  })

  describe('#register', () => {
    beforeEach(() => {
      when(UsersServiceMock.register(anything())).thenResolve({ access_token: 'access' })
    })

    it('calls userService.register()', async () => {
      await controller.register({ email: 'email', password: 'password', runningGoal: 0, cyclingGoal: 0, swimmingGoal: 0 })

      verify(UsersServiceMock.register(deepEqual({ email: 'email', password: 'password', runningGoal: 0, cyclingGoal: 0, swimmingGoal: 0 }))).once()
    })

    it('returns correct data', async () => {
      const result = await controller.register({ email: 'email', password: 'password', runningGoal: 0, cyclingGoal: 0, swimmingGoal: 0 })

      expect(result).deep.eq({ access_token: 'access' })
    })
  })

  describe('#updateUser', () => {
    beforeEach(() => {
      when(UsersServiceMock.update(anything(), anything())).thenResolve(userObject as any)
    })

    it('calls userService.update()', async () => {
      await controller.updateUser({ runningGoal: 5 }, { user: { userId: 'id', username: 'test' } })

      verify(UsersServiceMock.update(
        deepEqual({ userId: 'id', username: 'test' }),
        deepEqual({ runningGoal: 5 }))
      ).once()
    })

    it('returns correct data', async () => {
      const result = await controller.updateUser({ runningGoal: 5 }, { user: { userId: 'id', username: 'test' } })

      expect(result).deep.eq(userObject)
    })
  })

  describe('#addFriend', () => {
    beforeEach(() => {
      when(UsersServiceMock.addFriend(anything(), anything())).thenResolve(userObject as any)
    })

    it('calls userService.addFriend()', async () => {
      await controller.addFriend('123123', { user: { userId: 'id', username: 'test' } })

      verify(UsersServiceMock.addFriend(
        deepEqual({ userId: 'id', username: 'test' }),
        '123123'
      )).once()
    })

    it('returns correct data', async () => {
      const result = await controller.addFriend('123123', { user: { userId: 'id', username: 'test' } })

      expect(result).deep.eq(userObject)
    })
  })
})
