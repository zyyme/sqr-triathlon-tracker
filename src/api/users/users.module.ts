import { Module } from '@nestjs/common'
import { UsersService } from './users.service'
import { LocalStrategy } from '../../auth/local.strategy'
import { ConfigModule } from '@nestjs/config'
import { UsersController } from './users.controller'
import { LocalAuthGuard } from '../../auth/local-auth.guard'
import { AuthModule } from '../../auth/auth.module'
import { DbModule } from '../../db/db.module'

@Module({
  imports: [
    ConfigModule,
    DbModule,
    AuthModule
  ],
  controllers: [UsersController],
  providers: [
    UsersService
  ],
  exports: [UsersService]
})
export class UsersModule {}
