import { Inject, Injectable, NotFoundException } from '@nestjs/common'
import { UserClass, UserModel } from '../../schemas'
import { RegisterUserDto } from './dto/register-user.dto'
import * as bcrypt from 'bcrypt'
import { USER_MODEL } from '../../utils/injectTokens.utils'
import { AuthService } from '../../auth/auth.service'
import { UpdateUserDto } from './dto/update-user.dto'

@Injectable()
export class UsersService {
  constructor (
    @Inject(USER_MODEL) private readonly users: UserModel,
    private readonly authService: AuthService
  ) {
  }

  async getUser (id: string): Promise<Omit<UserClass, 'password'>> {
    return this.users.findById(id, { password: 0 })
  }

  async login (email: string, password: string) {
    return this.authService.login(await this.authService.validateUser(email, password))
  }

  async register (user: RegisterUserDto) {
    user.password = await bcrypt.hash(user.password, 10)

    const dbUser = await this.users.create(user)

    return this.authService.login(dbUser)
  }

  async update (user: { userId: string, username: string }, updateObject: UpdateUserDto) {
    await this.users.findByIdAndUpdate(user.userId, updateObject)

    return this.users.findById(user.userId)
  }

  async addFriend (user: { userId: string, username: string }, friendCode: string) {
    const friend = await this.users.findOne({ friendCode })

    if (friend) {
      const userObject = await this.users.findById(user.userId)
      userObject.friends.push(friend._id)
      await userObject.save()

      return userObject
    }

    throw new NotFoundException()
  }
}
