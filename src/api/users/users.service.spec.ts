import { UsersService } from './users.service'
import { mockBeforeEach } from '../../../test/test.utils'
import { UserModel } from '../../schemas'
import { AuthService } from '../../auth/auth.service'
import { anything, deepEqual, instance, verify, when } from 'ts-mockito'
import { expect } from 'chai'
import * as bcrypt from 'bcrypt'

describe('AuthService', () => {
  const UserModelMock = mockBeforeEach<UserModel>()
  const AuthServiceMock = mockBeforeEach<AuthService>()

  let service: UsersService

  let userObject

  beforeEach(() => {
    service = new UsersService(
      instance(UserModelMock),
      instance(AuthServiceMock)
    )

    userObject = {
      _id: 'id',
      email: 'email',
      name: 'name',
      runningGoal: 0,
      cyclingGoal: 0,
      swimmingGoal: 0,
      friendCode: '123123',
      friends: [],

      save: () => { return null }
    }
  })

  describe('#getUser', () => {
    beforeEach(() => {
      when(UserModelMock.findById(anything(), anything())).thenResolve(userObject as any)
    })

    it('calls userModel.findById()', async () => {
      await service.getUser('id')

      verify(UserModelMock.findById('id', deepEqual({ password: 0 }))).once()
    })

    it('returns correct data', async () => {
      const result = await service.getUser('id')

      expect(result).deep.eq(userObject)
    })
  })

  describe('#login', () => {
    beforeEach(() => {
      when(AuthServiceMock.login(anything())).thenResolve({ access_token: 'access' })
      when(AuthServiceMock.validateUser(anything(), anything())).thenResolve(userObject)
    })

    it('calls authService.login()', async () => {
      await service.login('email', 'password')

      verify(AuthServiceMock.login(deepEqual(userObject))).once()
    })

    it('calls authService.validateUser()', async () => {
      await service.login('email', 'password')

      verify(AuthServiceMock.validateUser('email', 'password')).once()
    })

    it('returns correct data', async () => {
      const result = await service.login('email', 'password')

      expect(result).deep.eq({ access_token: 'access' })
    })
  })

  describe('#register', () => {
    beforeEach(() => {
      when(UserModelMock.create(anything())).thenResolve(userObject as any)
      when(AuthServiceMock.login(anything())).thenResolve({ access_token: 'access' })
    })

    it('calls userModel.create()', async () => {
      await service.register({
        email: 'email',
        password: 'password',
        name: 'name',
        runningGoal: 0,
        cyclingGoal: 0,
        swimmingGoal: 0
      })

      verify(UserModelMock.create(anything())).once()
    })

    it('calls authService.login()', async () => {
      await service.register({
        email: 'email',
        password: 'password',
        name: 'name',
        runningGoal: 0,
        cyclingGoal: 0,
        swimmingGoal: 0
      })

      verify(AuthServiceMock.login(deepEqual(userObject))).once()
    })

    it('returns correct data', async () => {
      const result = await service.register({
        email: 'email',
        password: 'password',
        name: 'name',
        runningGoal: 0,
        cyclingGoal: 0,
        swimmingGoal: 0
      })

      expect(result).deep.eq({ access_token: 'access' })
    })
  })

  describe('#update', () => {
    beforeEach(() => {
      when(UserModelMock.findByIdAndUpdate(anything(), anything())).thenResolve()
      when(UserModelMock.findById(anything())).thenResolve(userObject as any)
    })

    it('calls userModel.findByIdAndUpdate', async () => {
      await service.update({ userId: 'id', username: 'name' }, { email: 'email2' })

      verify(UserModelMock.findByIdAndUpdate('id', deepEqual({ email: 'email2' }))).once()
    })

    it('calls userModel.findById', async () => {
      await service.update({ userId: 'id', username: 'name' }, { email: 'email2' })

      verify(UserModelMock.findById('id')).once()
    })

    it('returns correct data', async () => {
      const result = await service.update({ userId: 'id', username: 'name' }, { email: 'email2' })

      expect(result).deep.eq(userObject)
    })
  })

  describe('#addFriend', () => {
    beforeEach(() => {
      when(UserModelMock.findOne(anything())).thenResolve(userObject as any)
      when(UserModelMock.findById(anything())).thenResolve(userObject as any)
    })

    it('calls userModel.findOne()', async () => {
      await service.addFriend({ userId: 'id', username: 'name' }, '123123')

      verify(UserModelMock.findOne(deepEqual({ friendCode: '123123' })))
    })

    it('calls userModel.findById()', async () => {
      await service.addFriend({ userId: 'id', username: 'name' }, '123123')

      verify(UserModelMock.findById('id'))
    })

    it('throws if friend not found', async () => {
      when(UserModelMock.findOne(anything())).thenResolve(null)

      try {
        await service.addFriend({ userId: 'id', username: 'name' }, '123123')
      } catch (e) {
        expect(e.response.statusCode).eq(404)
        expect(e.response.message).eq('Not Found')
      }
    })

    it('returns correct data', async () => {
      const result = await service.addFriend({ userId: 'id', username: 'name' }, '123123')

      expect(result).deep.eq({
        ...userObject,
        friends: ['id']
      })
    })
  })
})
