import { Controller, Post, UseGuards, Body, Get, Req, Patch, Param } from '@nestjs/common'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { UsersService } from './users.service'
import { RegisterUserDto } from './dto/register-user.dto'
import { LoginUserDto } from './dto/login-user.dto'
import { JwtAuthGuard } from '../../auth/jwt-auth.guard'
import { UpdateUserDto } from './dto/update-user.dto'

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor (private readonly usersService: UsersService) {}

  @Get('')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  async getUser (@Req() req) {
    return this.usersService.getUser(req.user.userId)
  }

  @Post('login')
  async login (@Body() user: LoginUserDto) {
    return this.usersService.login(user.email, user.password)
  }

  @Post('register')
  async register (@Body() user: RegisterUserDto) {
    return this.usersService.register(user)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Patch()
  async updateUser (@Body() updateObject: UpdateUserDto, @Req() req) {
    return this.usersService.update(req.user, updateObject)
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Patch('add-friend/:friendCode')
  async addFriend (@Param('friendCode') friendCode: string, @Req() req) {
    return this.usersService.addFriend(req.user, friendCode)
  }
}
