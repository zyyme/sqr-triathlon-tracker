import { ApiProperty } from '@nestjs/swagger'
import { IsEmail, IsNumber, IsOptional, IsString, MinLength } from 'class-validator'

export class RegisterUserDto {
  @ApiProperty()
  @IsEmail()
  email: string

  @ApiProperty()
  @IsString()
  @MinLength(8)
  password: string

  @ApiProperty()
  @IsOptional()
  @IsString()
  name?: string

  @ApiProperty()
  @IsNumber()
  runningGoal: number

  @ApiProperty()
  @IsNumber()
  cyclingGoal: number

  @ApiProperty()
  @IsNumber()
  swimmingGoal: number
}
