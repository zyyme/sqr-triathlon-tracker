import * as mongoose from 'mongoose'
import { id } from '../utils/customId.utils'
import { DocumentType, getModelForClass, modelOptions, prop, ReturnModelType } from '@typegoose/typegoose'

export enum TrainingType {
  RUNNING = 'running',
  CYCLING = 'cycling',
  SWIMMING = 'swimming'
}

export enum TrainingUnits {
  IMPERIAL = 'IMPERIAL',
  METRIC = 'METRIC'
}

@modelOptions({ schemaOptions: { timestamps: true } })
export class TrainingClass {
  @prop(id)
  _id!: string

  @prop({ enum: TrainingType })
  type: TrainingType

  @prop()
  distance: number

  @prop()
  units: string

  @prop()
  userId: string
}

export type TrainingModel = ReturnModelType<typeof TrainingClass>
export type TrainingDocument = DocumentType<TrainingClass>

export function connectTrainingModel (connection: mongoose.Connection): TrainingModel {
  return getModelForClass(TrainingClass, {
    existingConnection: connection,
    schemaOptions: {
      collection: 'training'
    }
  })
}
