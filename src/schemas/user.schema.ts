import { id } from '../utils/customId.utils'
import { modelOptions, prop, ReturnModelType, DocumentType, getModelForClass, index } from '@typegoose/typegoose'
import mongoose from 'mongoose'
import { friendCode } from '../utils/friendCode.utils'

@index({ email: 1 }, { unique: true })
@modelOptions({ schemaOptions: { timestamps: true } })
export class UserClass {
  @prop(id)
  _id!: string

  @prop()
  email!: string

  @prop()
  password!: string

  @prop()
  name?: string

  @prop()
  runningGoal: number

  @prop()
  cyclingGoal: number

  @prop()
  swimmingGoal: number

  @prop(friendCode)
  friendCode: string

  @prop({ default: [] })
  friends: string[]
}

export type UserModel = ReturnModelType<typeof UserClass>
export type UserDocument = DocumentType<UserClass>

export function connectUserModel (connection: mongoose.Connection): UserModel {
  return getModelForClass(UserClass, {
    existingConnection: connection,
    schemaOptions: {
      collection: 'users'
    }
  })
}
