import { Inject, Injectable } from '@nestjs/common'
import { UserClass, UserModel } from '../schemas'
import * as bcrypt from 'bcrypt'
import { JwtService } from '@nestjs/jwt'
import { USER_MODEL } from '../utils/injectTokens.utils'

@Injectable()
export class AuthService {
  constructor (
    @Inject(USER_MODEL) private readonly users: UserModel,
    private jwtService: JwtService
  ) {}

  async validateUser (email: string, password: string): Promise<Omit<UserClass, 'password'>> {
    const user = await this.users.findOne({ email })

    if (user && await bcrypt.compare(password, user.password)) {
      return user
    }
  }

  async login (user: any) {
    const payload = { username: user.email, sub: user._id }
    return {
      access_token: this.jwtService.sign(payload)
    };
  }
}
